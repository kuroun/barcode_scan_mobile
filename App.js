import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';

export default class App extends React.Component {
  state = {
    hasCameraPermission: null
  }
  constructor(props){
    super(props)
    this.state ={
      read: false,
      product: {
        name: null,
        upc: null
      }
    }
  }
  
  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({hasCameraPermission: status === 'granted'});
    fetch('https://bar-code-scan-web.herokuapp.com/products.json')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson,
        });
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      if(this.state.read == false){
        return (
          <View style={{ flex: 1 }}>
            <BarCodeScanner
              onBarCodeRead={this._handleBarCodeRead}
              style={StyleSheet.absoluteFill}
            />
          </View>
        );
      }else{
        if(this.state.product.name == null){
          return(
            <View style={{marginTop: 200}}>
              <Text style={{color: 'red',fontWeight: 'bold',fontSize: 30, textAlign: 'center'}}>
                This product {this.state.product.upc} is not found!
              </Text>
            </View>
          );
        }else{
          return(
            <View style={{marginTop: 200}}>
              <Text style={{color: 'blue',fontWeight: 'bold',fontSize: 30, textAlign: 'center'}}>
                This product is found:{'\n'}
                {this.state.product.name}{'\n'}
                {this.state.product.upc}
              </Text>
            </View>
          );
        }

      }
    }
  }

  _handleBarCodeRead = ({ type, data }) => {
    var found_product = this.state.dataSource.find(function(product) {
      return product['upc'] == data;
    });

    if(found_product){
      this.setState({
        product: {
          name: found_product['name']
        }
      });
    }
    this.setState({
      read: true,
      product: {
        upc: data
      }
    });
  }
}